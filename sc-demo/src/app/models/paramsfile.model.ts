export class ParamsFile {
  features: Features[];
  beliefs: string;
  weightParameters: WeightParameters;
}

class Features {
  name: string;
  weight: number;
  aesthPos: number;
  aesthNeg: number;
  ethPos: number;
  ethNeg: number;
  epPos: number;
  epNeg: number;
  affPos: number;
  affNeg: number;
}

class WeightParameters {
  idtWeight: number;
  satisfactionWeight: number;
  similarityWeights: Weights;
  dissimilarityWeights: Weights;
  relevanceRowMapping: string;
  useIntentionsRowMapping: string;
  engagementRowMapping: string;
  decisionRowMapping: string;
  relevanceWeightMatrix: number[];
  useIntentionsWeightMatrix: number[];
  engagementWeightMatrix: number[];
  decisionWeightMatrix: number[];
}

class Weights {
  ethicsIndicative: number;
  ethicsCounter: number;
  affordancesIndicative: number;
  affordancesCounter: number;
  aestheticsIndicative: number;
  aestheticsCounter: number;
  epistemicsIndicative: number;
  epistemicsCounter: number;
}

