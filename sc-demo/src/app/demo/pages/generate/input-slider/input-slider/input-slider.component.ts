import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'input-slider',
  templateUrl: './input-slider.component.html',
  styleUrls: ['./input-slider.component.scss']
})
export class InputSliderComponent implements OnInit {

  parameterValue = 0;

  @Input() 
  get parameter() {
    return this.parameterValue;
  }

  @Input() name: string;
  
  @Output() parameterChange = new EventEmitter<number>();

  set parameter(val: number) {
    this.parameterValue = val;
    this.parameterChange.emit(this.parameterValue)
  }

  constructor() { }

  ngOnInit(): void {
  }

  showParameter() {
    console.log('waarde = ' + this.parameterValue);
  }

}
