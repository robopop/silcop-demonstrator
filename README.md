# Demonstrator for Silicon Coppélia

On a Mac:

1. git clone git@bitbucket.org:robopop/silcop-demonstrator.git
2. cd silcop-demonstrator
3. bin/ng-build.sh
4. bin/docker-compose-up.sh
5. point browser to http://localhost:4200

Test the proxy connection to the Silicon Coppélia server with

* curl -H "Content-Type: application/json" --request POST -d "@example-files/input.json" -sS "http://localhost:4200/api/appraise/test" | bin/yq.sh -y .
