import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HelpComponent } from './demo/pages/help/help.component';
import { AboutComponent } from './demo/pages/about/about.component';
import { GenerateComponent } from './demo/pages/generate/generate.component';


const routes: Routes = [
  { path: 'help', component: HelpComponent },
  { path: 'about', component: AboutComponent },
  { path: 'generate', component: GenerateComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
