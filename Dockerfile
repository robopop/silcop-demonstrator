FROM nginx

COPY sc-demo/dist/sc-demo /usr/share/nginx/html

EXPOSE 80
