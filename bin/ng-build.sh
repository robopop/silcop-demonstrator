#!/bin/bash

BIN="$(cd "$(dirname "$0")" ; pwd)"
PROJECT="$(dirname "${BIN}")"
SC_DEMO="${PROJECT}/sc-demo"

docker run -u $(id -u) --rm -ti -p 4200:4200 -v /etc/passwd:/etc/passwd -v "${SC_DEMO}":/app trion/ng-cli npm install
docker run -u $(id -u) --rm -ti -v "${SC_DEMO}":/app trion/ng-cli ng build
