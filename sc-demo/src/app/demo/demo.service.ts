import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { saveAs } from 'file-saver';


@Injectable({
  providedIn: 'root'
})
export class DemoService {
  private url = '/api/appraise/test';

  constructor(
    private http: HttpClient,
  ) { }

  async generateResponse(uploadedJSON: Object): Promise<any> {
    return await this.http.post<any>(
      `${this.url}`,
      uploadedJSON,
    ).toPromise();
  }

  async downloadOutput(uploadedJSON: Object): Promise<any> {
    let resp: HttpResponse<any> = await this.http.post<any>(
      `${this.url}`,
      uploadedJSON,
      { observe: 'response', responseType: 'blob' as 'json' },
    ).toPromise();
    let filename = 'response.json';
    var blob = new Blob([resp.body], { type: resp.headers.get("Content-Type") });
    saveAs(blob, filename);
  }
}
