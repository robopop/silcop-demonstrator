import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ControlContainer, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient, JsonpInterceptor } from '@angular/common/http';
import { DemoService } from '../../demo.service';
import { callbackify } from 'util';
import { stringify } from 'querystring';
import { ParamsFile } from '../../../models/paramsfile.model';
// import { ConsoleReporter } from 'jasmine';

@Component({
  selector: 'app-generate',
  templateUrl: './generate.component.html',
  styleUrls: ['./generate.component.scss'],
})

export class GenerateComponent implements OnInit {
  panelOpenState = false;
  selectedFileName: string;
  selectedFile: File;
  uploadForm: FormGroup;
  uploadedFileContent: any;
  uploadedJSON: object;
  generatedJSON: Blob;
  paramsFile: ParamsFile;
  paramsForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private demoService: DemoService,
  ) { }

  ngOnInit(): void {
    this.uploadForm = this.formBuilder.group({
      selectedFile: ['', [Validators.required]],
    });
    this.paramsForm = this.formBuilder.group({
      weightParameters: new FormControl(),
    });
  }

  onFileSelect(event: any) {
    if (event.target.files.length > 0) {

      const file = event.target.files[0];
      this.selectedFile = file;
      this.selectedFileName = file.name;

      let fileReader = new FileReader();

      fileReader.onload = ((file: any) => {
        return (e: Event) => {
          this.uploadedFileContent = fileReader.result;
          this.paramsFile = JSON.parse(this.uploadedFileContent);
        }
      })(file);

      fileReader.readAsText(file);

      }
    event.target.value = "";
  }

  async onGenerateResponse() {
    if (this.paramsFile) {
      // console.log(this.paramsFile.weightParameters.idtWeight);
      this.generatedJSON = await this.demoService.generateResponse(this.paramsFile);
    }
  }

  async onDownloadOutput() {
    if (this.paramsFile) {
      await this.demoService.downloadOutput(this.paramsFile);
    }
  }
}
